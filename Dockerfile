FROM debian:stretch
MAINTAINER Federico Gonzalez (https://github.com/fedeg/)

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y git build-essential curl gnupg --no-install-recommends \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install nodejs and npm
ENV NODE_VERSION 8.x
COPY setup_node_8.sh .
RUN bash setup_node_8.sh \
 && apt-get update \
 && apt-get install -y nodejs neovim vim \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN npm -g install babel-eslint eslint eslint-html-reporter eslint-import-resolver-node eslint-plugin-import eslint-plugin-jest eslint-plugin-react

COPY ./bundle /root/.vim/bundle
COPY ./.eslintrc.yaml /root/.eslintrc.yaml
COPY ./.vimrc /root/.vimrc
COPY ./autoload /root/.vim/autoload
RUN timeout 5m vim +PluginInstall +qall || true

CMD ["vim", "/src"]
